// WebGL - Fundamentals
// from https://webglfundamentals.org/webgl/webgl-fundamentals.html

/* eslint no-console:0 consistent-return:0 */
"use strict";


// m1 should be of size n1xn2 and m2 of size n2xn1
function multiplyMatrices(m1, m2, n1, n2) {
  var result = new Array(n1 * n1);
  for (var i = 0; i < n1; ++i) {
    for (var j = 0; j < n2; ++j) {
      var ind = i * n2 + j;
      result[ind] = 0;
      for (var k = 0; k < n2; ++k) {
        result[ind] += m1[i * n2 + k] * m2[k * n2 + j];
      }
    }
  }
  return result;
}

function multiplyM4(m1, m2) {
  var result = new Array(16);
  for (var ii = 0; ii < 4; ++ii) {
    for (var j = 0; j < 4; ++j) {
      var ind = ii * 4 + j;
      result[ind] = 0;
      for (var k = 0; k < 4; ++k) {
        result[ind] += m1[ii * 4 + k] * m2[k * 4 + j];
      }
    }
  }
  return result;
}

//rotatiom matrices. Angle is defined in radian

function rotx(angle) {
  var r = [
    1, 0, 0, 0,
    0, Math.cos(angle), -Math.sin(angle), 0,
    0, Math.sin(angle), Math.cos(angle), 0,
    0, 0, 0, 1,
  ];
  return r;
}

function roty(angle) {
  var r = [
    Math.cos(angle), 0, Math.sin(angle), 0,
    0, 1, 0, 0,
    -Math.sin(angle), 0, Math.cos(angle), 0,
    0, 0, 0, 1,
  ];
  return r;
}

function rotz(angle) {
  var r = [
    Math.cos(angle), -Math.sin(angle), 0, 0,
    Math.sin(angle), Math.cos(angle), 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1
  ];
  return r;
}

function scale(s1, s2, s3) {
  var s = [
    s1, 0, 0, 0,
    0, s2, 0, 0,
    0, 0, s3, 0,
    0, 0, 0, 1
  ];
  return s;
}

function translate(t1, t2, t3) {
  var t = [
    1, 0, 0, t1,
    0, 1, 0, t2,
    0, 0, 1, t3,
    0, 0, 0, 1
  ];
  return t;
}

function transpose(m) {
  var t = [
    m[0], m[4], m[8], m[12],
    m[1], m[5], m[9], m[13],
    m[2], m[6], m[10], m[14],
    m[3], m[7], m[11], m[15],
  ];
  return t;
}


function createShader(gl, type, source) {
  var shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (success) {
    return shader;
  }

  console.log(gl.getShaderInfoLog(shader));
  gl.deleteShader(shader);
}

function createProgram(gl, vertexShader, fragmentShader) {
  var program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  var success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (success) {
    return program;
  }

  console.log(gl.getProgramInfoLog(program));
  gl.deleteProgram(program);
}

async function main() {

  /******  INITIALISATION  ****/
    // Get A WebGL context
  var canvas = document.querySelector("#c");
  var gl = canvas.getContext("webgl");
  if (!gl) {
    return;
  }

  // Get the strings for our GLSL shaders
  // TODO : change here
  const vertexShaderResponse = await fetch('../glsl/vertex-shader-2d.glsl')
  const fragmentShaderResponse = await fetch('../glsl/fragment-shader-2d.glsl')
  var vertexShaderSource = await vertexShaderResponse.text();
  var fragmentShaderSource = await fragmentShaderResponse.text();
  // var vertexShaderSource = document.querySelector("#vertex-shader-2d").text;
  // var fragmentShaderSource = document.querySelector("#fragment-shader-2d").text;

  // create GLSL shaders, upload the GLSL source, compile the shaders
  var vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
  var fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);

  // Link the two shaders into a program
  var program = createProgram(gl, vertexShader, fragmentShader);

  webglUtils.resizeCanvasToDisplaySize(gl.canvas);

  // Tell WebGL how to convert from clip space to pixels
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

  // Clear the canvas
  gl.clearColor(0, 0, 0, 0);
  gl.clear(gl.COLOR_BUFFER_BIT);

  // Tell it to use our program (pair of shaders)
  gl.useProgram(program);
  /******* END INITIALIZATION  ***/


  /****  HANDLING AN ATTRIBUTE  ***/
    // look up where the vertex data needs to go.
  var positionAttributeLocation = gl.getAttribLocation(program, "a_position");

  // Create a buffer and put three 2d clip space points in it
  var positionBuffer = gl.createBuffer();

  // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  var positions = [
    [-1, -1],
    [-1, 1],
    [1, -1],
    [1, -1],
    [-1, 1],
    [1, 1],
  ];

  /*var positions = [
      [0, 0],
      [0, 2],
      [2, 0],
      [2, 0],
      [0, 2],
      [2, 2],
    ];*/

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions.flat()), gl.STATIC_DRAW);

  // Turn on the attribute
  gl.enableVertexAttribArray(positionAttributeLocation);

  // Bind the position buffer.
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
  var size = 2; // 2 components per iteration
  var type = gl.FLOAT; // the data is 32bit floats
  var normalize = false; // don't normalize the data
  var stride = 0; // 0 = move forward size * sizeof(type) each iteration to get the next position
  var offset = 0; // start at the beginning of the buffer
  gl.vertexAttribPointer(
    positionAttributeLocation, size, type, normalize, stride, offset);
  /*****  END HANDLING AN ATTRIBUTE  ***/


  /****  HANDLING AN ATTRIBUTE  ***/
    // look up where the vertex data needs to go.
  var colorAttributeLocation = gl.getAttribLocation(program, "a_color");

  // Create a buffer and put three 2d clip space points in it
  var colorBuffer = gl.createBuffer();

  // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
  gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

  var colors = [
    [0, 0, 1],
    [0, 0, 1],
    [1, 0, 0],
    [1, 0, 0],
    [0, 0, 1],
    [1, 0, 0],
  ];

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors.flat()), gl.STATIC_DRAW);

  // Turn on the attribute
  gl.enableVertexAttribArray(colorAttributeLocation);

  // Bind the position buffer.
  gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

  // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
  var size = 3; // 3 components per iteration
  var type = gl.FLOAT; // the data is 32bit floats
  var normalize = false; // don't normalize the data
  var stride = 0; // 0 = move forward size * sizeof(type) each iteration to get the next position
  var offset = 0; // start at the beginning of the buffer
  gl.vertexAttribPointer(
    colorAttributeLocation, size, type, normalize, stride, offset);
  /*****  END HANDLING AN ATTRIBUTE  ***/


  /****  HANDLING AN ATTRIBUTE  ***/
    // look up where the vertex data needs to go.
  var uvAttributeLocation = gl.getAttribLocation(program, "a_uv");

  // Create a buffer and put three 2d clip space points in it
  var uvBuffer = gl.createBuffer();

  // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
  gl.bindBuffer(gl.ARRAY_BUFFER, uvBuffer);

  var uvs = [
    [-1, -1],
    [-1, 1],
    [1, -1],
    [1, -1],
    [-1, 1],
    [1, 1],
  ];

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(uvs.flat()), gl.STATIC_DRAW);

  // Turn on the attribute
  gl.enableVertexAttribArray(uvAttributeLocation);

  // Bind the position buffer.
  gl.bindBuffer(gl.ARRAY_BUFFER, uvBuffer);

  // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
  var size = 2; // 3 components per iteration
  var type = gl.FLOAT; // the data is 32bit floats
  var normalize = false; // don't normalize the data
  var stride = 0; // 0 = move forward size * sizeof(type) each iteration to get the next position
  var offset = 0; // start at the beginning of the buffer
  gl.vertexAttribPointer(
    uvAttributeLocation, size, type, normalize, stride, offset);
  /*****  END HANDLING AN ATTRIBUTE  ***/

  /*** UNFIFORM ***/
  var sizeLocation = gl.getUniformLocation(program, "size");
  gl.uniform2f(sizeLocation, gl.canvas.width, gl.canvas.height);

  var radiusLocation = gl.getUniformLocation(program, "radius");
  gl.uniform1f(radiusLocation, 0.75);


  var centerLocation = gl.getUniformLocation(program, "center");
  gl.uniform2f(centerLocation, 0.0, 0.0);

  var trans = rotx(Math.PI);
  var transLocation = gl.getUniformLocation(program, "trans");
  gl.uniformMatrix4fv(transLocation, false, transpose(trans));

  /*** END UNFIFORM ***/

    // let boule = {
    //   pos: {
    //     x: 0,
    //     y: 0
    //   },
    //   dir: {
    //     x: 0.05,
    //     y: 0.01
    //   },
    //   radius: 0.1
    // }
    //
    // let boule2 = {
    //   pos: {
    //     x: 0,
    //     y: 0
    //   },
    //   dir: {
    //     x: -0.059,
    //     y: 0.02
    //   },
    //   radius: 0.1
    // }

  let boules = []

  for (let i = 0; i < 2; i++) {
    boules.push({
      pos: {
        x: (1.9 * Math.random() - 1),
        y: (1.9 * Math.random() - 1)
      },
      dir: {
        x: 0.01 * (2 * Math.random() - 1),
        y: 0.01 * (2 * Math.random() - 1)
      },
      radius: 0.2 * Math.random()
    })
  }

  function distanceBetweenCircles(b1, b2) {
    if (b1 === b2) return;
    return Math.sqrt((b2.pos.x - b1.pos.x) ** 2 + (b2.pos.y - b1.pos.y) ** 2);
  }

  // draw
  function drawScene() {
    var primitiveType = gl.TRIANGLES;
    var offset = 0;
    var count = 6;

    boules.forEach(boule => {

      if ((Math.abs(boule.pos.x) + boule.radius) >= 1) boule.dir.x = -boule.dir.x;
      if ((Math.abs(boule.pos.y) + boule.radius) >= 1) boule.dir.y = -boule.dir.y;


      boule.pos.x += boule.dir.x;
      boule.pos.y += boule.dir.y;

      boules.forEach(boule2 => {
        let distanceCircle = distanceBetweenCircles(boule, boule2)
        if (distanceCircle < boule.radius + boule2.radius) {
          let angle = Math.atan2(boule2.pos.y - boule.pos.y, boule2.pos.x - boule.pos.x);
          let distanceToMove = boule.radius + boule2.radius - distanceCircle;

          boule2.pos.x += Math.cos(angle) * distanceToMove;
          boule2.pos.y += Math.sin(angle) * distanceToMove;

          let tangentVector = {
            x: -(boule2.pos.x - boule.pos.x),
            y: boule2.pos.y - boule.pos.y
          }
          console.log('tangentVector before', tangentVector)
          let distTangentVector = Math.sqrt(tangentVector.x ** 2 + tangentVector.y ** 2)
          tangentVector.x /= distTangentVector;
          tangentVector.y /= distTangentVector;
          console.log('tangentVector after', tangentVector)

          let relativeVelocity = {
            x: boule2.dir.x - boule.dir.x,
            y: boule2.dir.y - boule.dir.y
          }
          console.log('relativeVelocity', relativeVelocity)

          let length = relativeVelocity.x * tangentVector.x + relativeVelocity.y + tangentVector.y;
          console.log('length', length)
          let velocityComponentOnTangent = {
            x: tangentVector.x * length,
            y: tangentVector.y * length
          }
          console.log('velocityComponentOnTangent', velocityComponentOnTangent)

          let velocityComponentPerpendicularToTangent = {
            x: relativeVelocity.x - velocityComponentOnTangent.x,
            y: relativeVelocity.y - velocityComponentOnTangent.y
          }
          console.log('velocityComponentPerpendicularToTangent', velocityComponentPerpendicularToTangent)
          boule.dir.x -= velocityComponentPerpendicularToTangent.x
          boule.dir.y -= velocityComponentPerpendicularToTangent.y

          boule2.dir.x += velocityComponentPerpendicularToTangent.x
          boule2.dir.y += velocityComponentPerpendicularToTangent.y


          boule.pos.x += boule.dir.x;
          boule.pos.y += boule.dir.y;


          boule2.pos.x += boule2.dir.x;
          boule2.pos.y += boule2.dir.y;
        }
      });

      gl.uniform2f(centerLocation, boule.pos.x, boule.pos.y);
      gl.uniform1f(radiusLocation, boule.radius);
      gl.drawArrays(primitiveType, offset, count);

    })

    requestAnimationFrame(drawScene)


  }


  requestAnimationFrame(drawScene)
}

main();


// an attribute will receive data from a buffer
attribute vec4 a_position;
attribute vec3 a_color;
//attribute vec2 a_uv;

uniform vec2 size;
uniform float radius;
uniform vec2 center;
uniform mat4 trans;

varying vec3 fragColor;
varying vec2 fragUV;

// all shaders have a main function
void main() {

    // gl_Position is a special variable a vertex shader
    // is responsible for setting
    gl_Position = a_position;
    gl_Position.xy *= radius;
    gl_Position.xy += center;
    fragUV = a_position.xy;
    gl_Position.y *= size.x/size.y;
    fragColor = a_color;
}

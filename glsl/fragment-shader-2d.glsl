// fragment shaders don't have a default precision so we need
// to pick one. mediump is a good default
precision mediump float;

varying vec3 fragColor;
varying vec2 fragUV;

void main() {
    // gl_FragColor is a special variable a fragment shader
    // is responsible for setting
    float ll = fragUV.x*fragUV.x + fragUV.y*fragUV.y;
    if (ll > 1.0) {
        discard;
    }

    gl_FragColor = vec4(fragColor, 1);
}
